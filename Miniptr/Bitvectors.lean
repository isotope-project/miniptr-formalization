
abbrev Bit := Fin 2

abbrev Bitvector (n: Nat): Type := Fin n -> Bit

instance bitvector_eq_decidable {n}: DecidableEq (Bitvector n) 
  := sorry

def Bitvector.zero (n: Nat): Bitvector n := λ_ => 0

abbrev Bits: Type := Nat -> Bit

def Bits.zero: Bits := λ_ => 0

def Bitvector.to_big {n: Nat} (b: Bitvector n): Bits
  := λk =>
    if p: k < n then b ⟨k, p⟩ else 0

instance bitvector_to_big {n: Nat}: Coe (Bitvector n) Bits := {
  coe := Bitvector.to_big
}

def Bits.to_small (b: Bits) (n: Nat): Bitvector n
  := λ⟨k, _⟩ => b k

def Bits.truncate (b: Bits) (n: Nat): Bits
  := λk => if k < n then b k else 0

def Bitvector.truncate {n: Nat} (b: Bitvector n) (m: Nat): Bitvector n
  := λe => if e.val < m then b e else 0

def Bitvector.cast {n: Nat} (b: Bitvector n) (m: Nat): Bitvector m
  := b.to_big.to_small m

def Bits.is_prefix (b b': Bits) (n: Nat): Prop := ∀k: Nat, k < n -> b k = b' k

theorem Bits.is_prefix_zero (b b': Bits): b.is_prefix b' 0 
  := by intro k p; cases p

def Bits.is_prefix_vec {n: Nat} (b: Bits) (b': Bitvector n): Prop := b.is_prefix b' n

theorem Bits.is_prefix_is_small_eq (b b': Bits) (n: Nat):
  b.is_prefix b' n = (b.to_small n = b'.to_small n)
  := by {
    apply propext;
    apply Iff.intro;
    {
      intro H;
      simp only [to_small];
      funext ⟨x, p⟩;
      apply H;
      exact p;
    }
    {
      simp only [to_small, Bits.is_prefix];
      intro H k p;
      have R: ∀b: Bits, (λe => b e.val) (Fin.mk k p) = b k := by intros; rfl;
      rw [<-R b, <-R b', H]
    }
  }

theorem Bits.is_prefix_refl (b: Bits): ∀n: Nat, b.is_prefix b n
  := by intros _ _ _; rfl

theorem Bits.is_prefix.trans {a b c: Bits} {n}
  : (a.is_prefix b n) -> (b.is_prefix c n) -> a.is_prefix c n
  := by {
    simp only [Bits.is_prefix_is_small_eq]
    exact Eq.trans
  }

theorem Bits.is_prefix.symm {b b': Bits} {n}: (b.is_prefix b' n) -> (b'.is_prefix b n)
  := by {
    simp only [Bits.is_prefix_is_small_eq]
    exact Eq.symm
  }

instance bits_prefix_decidable {b b': Bits} {n}: Decidable (b.is_prefix b' n) 
  := sorry

def Bits.shl (b: Bits) (n: Nat): Bits := λk: Nat => b (n + k)

@[simp]
theorem Bits.to_small_to_big_truncate (b: Bits) (n: Nat):
  (b.to_small n).to_big = b.truncate n
  := by {
    simp only [Bitvector.to_big, truncate];
    funext k;
    split <;> rfl
  }

@[simp]
theorem Bitvector.to_big_to_small_cast {n} (b: Bitvector n) (m: Nat):
  b.to_big.to_small m = b.cast m
  := rfl


@[simp]
theorem Bitvector.cast_self {n} (b: Bitvector n):
  b.cast n = b
  := by {
    simp only [cast, to_big, Bits.to_small]
    funext ⟨x, p⟩;
    split;
    rfl;
    contradiction
  }

theorem Bitvector.truncate_cast_commute {k} (b: Bitvector k) (n m: Nat):
  (b.truncate n).cast m = (b.cast m).truncate n
  := by {
    simp only [truncate, cast, Bits.to_small, to_big];
    funext x;
    repeat first | rfl | split
  }

@[simp]
theorem Bitvector.truncate_to_big_commute {n} (b: Bitvector n) (m: Nat):
  (b.truncate m).to_big = b.to_big.truncate m
  := by {
    simp only [truncate, Bits.truncate, to_big];
    funext k;
    repeat first | rfl | split
  }

abbrev Bitvector.is_prefix {n m} (b: Bitvector n) (b': Bitvector m) (k: Nat): Prop 
  := b.to_big.is_prefix b'.to_big k

theorem Bitvector.is_prefix_is_cast_eq {n} (b b': Bitvector n) (m: Nat):
  b.is_prefix b' m = (b.cast m = b'.cast m)
  := by {
    rw [Bitvector.is_prefix, Bitvector.cast, Bits.is_prefix_is_small_eq]
    rfl
  }

theorem Bitvector.is_prefix_refl (b: Bitvector k): ∀n: Nat, b.is_prefix b n
  := by intros _ _ _; rfl

theorem Bitvector.is_prefix.trans {k} {a b c: Bitvector k} {n}
  : (a.is_prefix b n) -> (b.is_prefix c n) -> a.is_prefix c n
  := by {
    simp only [Bitvector.is_prefix_is_cast_eq]
    exact Eq.trans
  }

theorem Bitvector.is_prefix.symm {k} {b b': Bitvector k} {n}
  : (b.is_prefix b' n) -> (b'.is_prefix b n)
  := by {
    simp only [Bitvector.is_prefix_is_cast_eq]
    exact Eq.symm
  }

abbrev Bitvector.is_prefix_vec {n m} (b: Bitvector n) (p: Bitvector m): Prop 
  := b.is_prefix p (Nat.min n m)

def Bitvector.shl {n} (b: Bitvector n) (m: Nat): Bitvector (n - m) 
  := (b.to_big.shl m).to_small (n - m)

def Bitvector.segment {n} (b: Bitvector n) (m: Nat): Bitvector m × Bitvector (n - m)
  := (b.cast m, b.shl m)
