import Std.Classes.SetNotation
import Std.Logic

def Set (α: Type _): Type _ := α → Prop

instance bitset_membership {α}: Membership α (Set α) := {
  mem := λa s => s a
}

instance bitset_empty {α}: EmptyCollection (Set α) := {
  emptyCollection := λ_ => False
}

def Set.full {α}: Set α := λ_ => True

instance set_subset {α}: HasSubset (Set α) := {
  Subset := λbs bs' => ∀{b}, b ∈ bs -> b ∈ bs'
}

theorem Set.subset_def {α} (l r: Set α) (a: α): l ⊆ r -> a ∈ l -> a ∈ r 
  := λH => H

theorem Set.subset_empty {α} (bs: Set α): ∅ ⊆ bs := λH => by cases H

theorem Set.subset_full {α} (bs: Set α): bs ⊆ Set.full := λ_ => True.intro

theorem Set.subset_refl (bs: Set α): bs ⊆ bs := λH => H

theorem Set.subset_trans (a b c: Set α):
  a ⊆ b -> b ⊆ c -> a ⊆ c
  := λHab Hbc _ Ha => Hbc (Hab Ha)

theorem Set.subset_antisym {bs bs': Set α}
  : bs ⊆ bs' -> bs' ⊆ bs -> bs = bs' 
  := by {
  intro H H';
  funext b;
  exact (propext (Iff.intro H H'))
}

instance set_union {α}: Union (Set α) := {
  union := λbs bs' b => b ∈ bs ∨ b ∈ bs'
}

@[simp]
theorem Set.union_comm {bs bs': Set α}
: bs ∪ bs' = bs' ∪ bs 
:= by {
  funext b;
  simp [Union.union, or_comm]
}

theorem Set.union_is_join {bs bs' ms: Set α}
  : bs ⊆ ms -> bs' ⊆ ms -> bs ∪ bs' ⊆ ms 
  := by {
    intro H H' b H'';
    cases H'' with
    | inl Hl => exact H Hl
    | inr Hr => exact H' Hr
  }

theorem Set.union_subset_left {bs bs': Set α}
  : bs ⊆ bs ∪ bs'
  := λHb => Or.inl Hb

theorem Set.union_subset_right {bs bs': Set α}
  : bs' ⊆ bs ∪ bs'
  := λHb => Or.inr Hb

@[simp]
theorem Set.union_empty_left {bs: Set α}
  : ∅ ∪ bs = bs
  := subset_antisym 
    (λH => match H with | Or.inl H => H.elim | Or.inr H => H) 
    union_subset_right

@[simp]
theorem Set.union_empty_right {bs: Set α}
  : bs ∪ ∅ = bs
  := union_comm ▸ union_empty_left

@[simp]
theorem Set.union_full_left {bs: Set α}
  : Set.full ∪ bs = Set.full
  := subset_antisym (subset_full _) (λ_ => Or.inl True.intro)

@[simp]
theorem Set.union_full_right {bs: Set α}
  : bs ∪ Set.full = Set.full
  := union_comm ▸ union_full_left

@[simp]
theorem Set.union_self {bs: Set α}
  : bs ∪ bs = bs
  := subset_antisym
    (λH => by cases H <;> assumption)
    union_subset_left

instance set_intersect {α}: Inter (Set α) := {
  inter := λbs bs' b => b ∈ bs ∧ b ∈ bs'
}

@[simp]
theorem Set.intersect_comm {bs bs': Set α}
: bs ∩ bs' = bs' ∩ bs 
:= by {
  funext b;
  simp [Inter.inter, and_comm]
}

theorem Set.intersect_is_meet {bs bs' ms: Set α}
  : ms ⊆ bs -> ms ⊆ bs' -> ms ⊆ bs ∩ bs' 
  := λH H' _ Hb => ⟨H Hb, H' Hb⟩

theorem Set.intersect_subset_left {bs bs': Set α}
  : bs ∩ bs' ⊆ bs
  := λHb => Hb.left

theorem Set.intersect_subset_right {bs bs': Set α}
  : bs ∩ bs' ⊆ bs'
  := λHb => Hb.right

@[simp]
theorem Set.intersect_empty_left {bs: Set α}
  : ∅ ∩ bs = ∅
  := subset_antisym (λH => H.left.elim) (subset_empty _)

@[simp]
theorem Set.intersect_empty_right {bs: Set α}
  : bs ∩ ∅ = ∅
  := intersect_comm ▸ intersect_empty_left

@[simp]
theorem Set.intersect_full_left {bs: Set α}
  : Set.full ∩ bs = bs
  := subset_antisym (λH => H.right) (λH => ⟨True.intro, H⟩)

@[simp]
theorem Set.intersect_full_right {bs: Set α}
  : bs ∩ Set.full = bs
  := intersect_comm ▸ intersect_full_left

instance set_strict_subset {α}: HasSSubset (Set α) := {
  SSubset := λbs bs' => bs ⊆ bs' ∧ bs ≠ bs'
}

theorem Set.strict_subset_of_subset_and_not_superset {α} {bs bs': Set α}:
  bs ⊆ bs' → ¬(bs ⊇ bs') -> bs ⊂ bs'
  := λH1 H2 => ⟨H1, λHeq => H2 (Heq ▸ (subset_refl _))⟩

theorem Set.not_superset_of_strict_subset {α} {bs bs': Set α}:
  bs ⊂ bs' -> ¬(bs ⊇ bs')
  := λ⟨Hss, Hne⟩ H2 => Hne (subset_antisym Hss H2)

theorem Set.subset_not_superset_is_strict_subset {α} (bs bs': Set α):
  (bs ⊆ bs' ∧ ¬(bs ⊇ bs')) = (bs ⊂ bs')
  := propext (Iff.intro 
    (λ⟨H1, H2⟩ => strict_subset_of_subset_and_not_superset H1 H2) 
    (λH => ⟨H.left, not_superset_of_strict_subset H⟩))