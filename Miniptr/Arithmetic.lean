theorem le_partial_ord {n m: Nat}: n ≤ m -> m ≤ n -> n = m := by {
  sorry
}

theorem le_total_not {n m: Nat}: ¬(n ≤ m) -> ¬(m ≤ n) -> False := by {
  intros Hc Hc';
  cases Nat.le_total n m <;> contradiction
}

theorem max_comm {n m}: Nat.max n m = Nat.max m n := by {
  simp only [Nat.max_def]
  split
  split
  apply le_partial_ord <;> assumption
  rfl
  split
  rfl
  apply False.elim
  apply le_total_not <;> assumption
}

theorem left_le_max {n m}: n ≤ Nat.max n m := by {
  simp only [Nat.max_def]
  split
  assumption
  apply Nat.le_refl
}

theorem right_le_max {n m}: m ≤ Nat.max n m := max_comm ▸ left_le_max

theorem max_le_split {l r l' r'}: l ≤ l' -> r ≤ r' -> Nat.max l r ≤ Nat.max l' r' := by {
  intro Hl Hr;
  simp only [Nat.max_def]
  split
  split
  exact Hr
  apply Nat.le_trans Hr
  apply Nat.le_of_lt
  apply Nat.gt_of_not_le
  assumption
  split
  apply Nat.le_trans Hl
  assumption
  exact Hl
}

theorem max_lt_split {l r l' r'}: l < l' -> r < r' -> Nat.max l r < Nat.max l' r' := by {
  intro Hl Hr;
  simp only [Nat.max_def]
  split
  split
  exact Hr
  apply Nat.le_trans Hr
  apply Nat.le_of_lt
  apply Nat.gt_of_not_le
  assumption
  split
  apply Nat.le_trans Hl
  assumption
  exact Hl
}

theorem sub_one_lt_helper_proof {n m r}: n > 0 → n ≤ r + 1 → n - (m + 1) ≤ r := by {
  intro Hn;
  rw [Nat.sub_succ]
  sorry
}