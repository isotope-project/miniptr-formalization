import Std.Classes.SetNotation
import Std.Logic
import Miniptr.Bitvectors
import Miniptr.Arithmetic
import Miniptr.Set

structure LeafTrie :=
  (len: Nat)
  (bits: Bitvector len)

instance leaf_trie_eq_decidable: DecidableEq LeafTrie 
  := sorry

def LeafTrie.bitset (l: LeafTrie): Set Bits := λb => b.is_prefix_vec l.bits

def LeafTrie.shl (l: LeafTrie) (n: Nat): LeafTrie := {
  len := l.len - n
  bits := l.bits.shl n
}

def LeafTrie.shl_len_def (l: LeafTrie) (n: Nat): (l.shl n).len = l.len - n := rfl

def LeafTrie.segment (l: LeafTrie) (n: Nat): Bitvector n × LeafTrie
  := 
    let (lw, lr) := l.bits.segment n;
    (lw, { len := l.len - n, bits := lr }) 

instance leaf_trie_subset: HasSubset LeafTrie := {
  Subset := λl r => l.bitset ⊆ r.bitset
}

def leaf_trie_subset_check (l r: LeafTrie): Prop
  := r.len ≤ l.len ∧ r.bits.to_big.is_prefix l.bits.to_big r.len

theorem LeafTrie.subset_check_conservative {l r: LeafTrie}: leaf_trie_subset_check l r → l ⊆ r := by {
    intro ⟨Hlr, Hlr'⟩ b bsl _ Hn;
    simp only at *;
    rw [bsl _ (Nat.lt_of_lt_of_le Hn Hlr)]
    rw [Hlr' _ Hn]
}

theorem LeafTrie.subset_check_complete {l r: LeafTrie}: l ⊆ r → leaf_trie_subset_check l r := by {
  cases l with
  | mk ll bl =>
    cases r with
    | mk lr br =>
      simp only [bitset]
      intro Hlr;
      apply And.intro;
      sorry
      sorry
}

theorem LeafTrie.subset_check_correct {l r: LeafTrie}: leaf_trie_subset_check l r = (l ⊆ r) 
  := propext (Iff.intro subset_check_conservative subset_check_complete)

instance leaf_trie_subset_decidable (l r: LeafTrie): Decidable (l ⊆ r) := sorry

theorem LeafTrie.canonical {l r: LeafTrie}: l.bitset = r.bitset -> l = r := sorry

theorem LeafTrie.subset_refl (l: LeafTrie): l ⊆ l := Set.subset_refl _

theorem LeafTrie.subset_antisym {l r: LeafTrie}: l ⊆ r -> r ⊆ l -> l = r 
  := λHlr Hrl => canonical (Set.subset_antisym Hlr Hrl)

theorem LeafTrie.everyone_subset_zero (l r: LeafTrie): (r.len = 0) -> (l ⊆ r) := by {
  sorry
  -- intro Hr;
  -- cases r with
  -- | mk len bits =>
  --   cases len with
  --   | zero => 
  --     simp only [leaf_trie_subset, Subset]
  --     exact ⟨Nat.zero_le _, Bits.is_prefix_zero _ _⟩
  --   | succ n => cases Hr
}

theorem LeafTrie.zero_eq {l r: LeafTrie}: l.len = 0 -> r.len = 0 -> l = r := by {
  intros;
  apply subset_antisym <;>
  apply everyone_subset_zero <;>
  assumption
}

theorem LeafTrie.not_subset_not_zero {l r: LeafTrie}: ¬(l ⊆ r) -> r.len ≠ 0 := by {
  intro Hn Hr;
  exact Hn (l.everyone_subset_zero r Hr)
}

theorem LeafTrie.not_subset_gt_zero {l r: LeafTrie}: ¬(l ⊆ r) -> 0 < r.len := by {
  intro Hn;
  apply Nat.zero_lt_of_ne_zero;
  apply not_subset_not_zero Hn;
}

inductive Trie (b: Nat)
  | empty: Trie b
  | leaf: LeafTrie -> Trie b
  | branch: (Bitvector (b + 1) -> Trie b) -> Trie b

instance trie_eq_decidable {b}: DecidableEq (Trie b) := sorry

inductive Trie.bitset {w}: Trie w -> Set Bits
  | leaf l b: (b ∈ l.bitset) -> (Trie.leaf l).bitset b
  | branch bs b: (bs (b.to_small (w + 1))).bitset (b.shl (w + 1)) -> (Trie.branch bs).bitset b

theorem Trie.bitset_leaf {l w}: @bitset w (leaf l) = l.bitset
  := by {
    funext b;
    apply propext;
    apply Iff.intro;
    intro H
    cases H
    assumption
    intro H
    constructor
    assumption
  }

theorem Trie.bitset_empty {w}: @bitset w (empty) = λ_ => False
  := by {
    funext b;
    apply propext;
    apply iff_of_false <;>
    intro H <;> 
    cases H
  }

instance leaf_to_trie {w}: Coe LeafTrie (Trie w) := {
  coe := Trie.leaf
}

def LeafTrie.union (l r: LeafTrie) (w: Nat): Trie w 
  := if p: l ⊆ r then r else if q: r ⊆ l then l else
      Trie.branch (λb => 
        if b.is_prefix_vec l.bits then
          if b.is_prefix_vec r.bits then
            have : (Nat.max (l.len - (w + 1)) (r.len - (w + 1))) < Nat.max l.len r.len 
              := by {
                apply max_lt_split 
                <;> apply Nat.sub_lt
                <;> first
                | (apply not_subset_gt_zero <;> assumption)
                | apply Nat.zero_lt_succ
              }
            union (l.shl (w + 1)) (r.shl (w + 1)) w
          else 
            l
        else if b.is_prefix_vec r.bits then r
        else Trie.empty
      )
    termination_by union l r _ => Nat.max l.len r.len

theorem LeafTrie.union_zero_left {l r: LeafTrie}
  : l.len = 0 -> l.union r w = l
  := by {
    intro Hl;
    unfold LeafTrie.union;
    split
    {
      apply congr rfl
      apply subset_antisym (everyone_subset_zero _ _ Hl)
      assumption
    }
    {
      split
      {
        rfl
      }
      {
        --TODO: report bug that this gives a type error!
        -- have Hl := everyone_subset_zero _ _ Hl;
        have c: ∀A, A → ¬A → False := λ_ a n => n a;
        apply c (r ⊆ l);
        apply everyone_subset_zero;
        assumption
        assumption
      }
    }
  }

theorem LeafTrie.union_zero_right {l r: LeafTrie}
  : r.len = 0 -> l.union r w = r
  := by {
    have c: ∀A, A → ¬A → False := λ_ a n => n a;
    intro Hr;
    unfold LeafTrie.union;
    split
    rfl
    split <;>
    (
      apply False.elim;
      apply c (l ⊆ r);
      apply everyone_subset_zero;
      assumption
      assumption
    )
  }

theorem LeafTrie.union_idem  (l: LeafTrie) {w}
  : l.union l w = l
  := by {
    have c: ∀A, A → ¬A → False := λ_ a n => n a;
    unfold LeafTrie.union;
    split
    rfl
    --TODO: report inference bug...
    apply c (l ⊆ l);
    apply subset_refl;
    assumption
  }

theorem LeafTrie.union_comm_helper {n} {l r: LeafTrie} {w}
  : l.len ≤ n -> r.len ≤ n ->
  (l.union r w) = (r.union l w) 
  := by {
    induction n generalizing l r with
    | zero => 
      intro _ Hr;
      rw [union_zero_left (Nat.eq_zero_of_le_zero Hr)]
      rw [union_zero_right (Nat.eq_zero_of_le_zero Hr)]
    | succ n I =>
      intro Hl Hr
      unfold LeafTrie.union;
      split
      split
      apply congr rfl
      apply subset_antisym <;> assumption
      rfl
      split
      rfl
      apply congr rfl
      funext b
      split
      split
      dsimp only []
      apply I 
      <;> rw [shl_len_def]
      <;> apply sub_one_lt_helper_proof
      <;> first 
        | assumption 
        | (apply LeafTrie.not_subset_gt_zero <;> assumption)
      rfl
      rfl
  }

theorem LeafTrie.union_comm {l r: LeafTrie} {w}
  : (l.union r w) = (r.union l w) 
  := by {
    apply @LeafTrie.union_comm_helper (Nat.max l.len r.len)
    apply left_le_max;
    apply right_le_max;
  }

theorem LeafTrie.union_left_subset_helper {n} {l r: LeafTrie} {w}
  : l.len ≤ n -> r.len ≤ n -> 
  l.bitset ⊆ (l.union r w).bitset
  := by {
    induction n generalizing l r with
    | zero => 
      intro Hl Hr
      rw [union_zero_left (Nat.eq_zero_of_le_zero Hl)]
      rw [Trie.bitset_leaf]
      apply Set.subset_refl
    | succ n I =>
      cases l with
      | mk ll lb =>
        cases r with
        | mk rl rb =>
          unfold union
          simp only [bitset, shl]
          intro Hll Hrl b bs
          split
          {
            constructor
            apply Set.subset_def
            assumption
            exact bs
          }
          {
            split
            {
              constructor
              exact bs
            }
            {
              constructor
              split
              {
                split
                {
                  apply I;
                  sorry
                  sorry
                  sorry
                }
                {
                  constructor
                  sorry
                }
              }
              {
                split
                {
                  constructor
                  sorry
                }
                {
                  sorry
                }
              }
            }
          }
  }

theorem LeafTrie.union_left_subset {l r: LeafTrie} {w}
  : l.bitset ⊆ (l.union r w).bitset
  := by {
    apply @LeafTrie.union_left_subset_helper (Nat.max l.len r.len)
    apply left_le_max;
    apply right_le_max;
  }

theorem LeafTrie.union_right_subset {l r: LeafTrie} {w}
  : r.bitset ⊆ (l.union r w).bitset
  := union_comm ▸ union_left_subset

theorem LeafTrie.union_complete {l r: LeafTrie} {w}
  : l.bitset ∪ r.bitset ⊆ (l.union r w).bitset
  := Set.union_is_join union_left_subset union_right_subset

theorem LeafTrie.union_conservative_helper {n} {l r: LeafTrie} {w}
  : l.len ≤ n → r.len ≤ n →
  (l.union r w).bitset ⊆ l.bitset ∪ r.bitset
  := sorry

theorem LeafTrie.union_conservative {l r: LeafTrie} {w}
  : (l.union r w).bitset ⊆ l.bitset ∪ r.bitset
  := by {
    apply @LeafTrie.union_conservative_helper (Nat.max l.len r.len)
    apply left_le_max;
    apply right_le_max;
  }

theorem LeafTrie.union_correct {l r: LeafTrie} {w}
  : (l.union r w).bitset = l.bitset ∪ r.bitset 
  := Set.subset_antisym union_conservative union_complete

def LeafTrie.union_branch {w} (l: LeafTrie) (bs: Bitvector (w + 1) -> Trie w)
  : Trie w 
  := sorry

def LeafTrie.union_trie {w} (l: LeafTrie): Trie w -> Trie w
  | Trie.empty => Trie.leaf l
  | Trie.leaf r => l.union r w
  | Trie.branch bs => l.union_branch bs

def Trie.union {w}: Trie w -> Trie w -> Trie w
  | Trie.empty, t | t, Trie.empty => t
  | Trie.leaf l, t | t, Trie.leaf l => l.union_trie t
  | Trie.branch l, Trie.branch r => Trie.branch (λb => (l b).union (r b))

instance trie_union {w}: Union (Trie w) := {
  union := Trie.union
}

instance trie_subset {w}: HasSubset (Trie w) := {
  Subset := λl r => l.bitset ⊆ r.bitset
}

theorem Trie.union_comm {w} {l r: Trie w}: l ∪ r = r ∪ l := by {
  induction l generalizing r with
  | empty => cases r <;> rfl
  | leaf l =>
    cases r with
    | empty => rfl
    | leaf => simp only [Union.union, Trie.union, LeafTrie.union_trie, LeafTrie.union_comm]
    | branch => simp only [Union.union, Trie.union, LeafTrie.union_trie]
  | branch lb Ilb => 
    cases r with
    | empty => rfl
    | leaf => simp only [Union.union, Trie.union, LeafTrie.union_trie]
    | branch rb => 
      simp only [Union.union, Trie.union]
      apply congr rfl
      funext b
      apply Ilb
}

theorem Trie.union_leaf_left {w} {l: LeafTrie} {r: Trie w}: (leaf l) ⊆ (leaf l) ∪ r 
  := sorry

theorem Trie.union_leaf_right {w} {l: Trie w} {r: LeafTrie}: (leaf r) ⊆ l ∪ (leaf r) 
  := union_comm ▸ union_leaf_left

theorem Trie.union_left {w} {l r: Trie w}: l ⊆ l ∪ r := by {
  induction l generalizing r with
  | empty => 
    cases r <;> 
    sorry -- Empty is a subset of everything
  | leaf l => sorry
  | branch lb Ilb => sorry
}

theorem Trie.union_right {w} {l r: Trie w}: r ⊆ l ∪ r 
  := union_comm ▸ union_left

theorem Trie.union_complete {w} {l r: Trie w}
  : l.bitset ∪ r.bitset ⊆ (l ∪ r).bitset
  := Set.union_is_join union_left union_right

theorem Trie.union_conservative {w} {l r: Trie w}
  : (l ∪ r).bitset ⊆ l.bitset ∪ r.bitset 
  := sorry

theorem Trie.union_correct {w} {l r: Trie w}
  : (l ∪ r).bitset = l.bitset ∪ r.bitset 
  := Set.subset_antisym union_conservative union_complete